import React from "react";
import Modal from "./Modal";
function Section() {
  return (
    <div className="bg-primary p-5">
      <div className="container">
        <div className="row">
        <div className="col-md-6 offset-md-3">
          <Modal />
        </div>
        </div>
      </div>
    </div>
    
  );
}

export default Section;
