import React from "react";
import { Spinner, Row, Col, Container } from "reactstrap";

const ExampleSpinners = (props) => {
  return (
    <Container>
      <Row className="text-center">
        <Col md={{ size: 6, offset: 3 }}>
          <Spinner className="m-2" type="grow" color="primary" />
          <Spinner className="m-2" type="grow" color="secondary" />
          <Spinner className="m-2" type="grow" color="success" />
          <Spinner className="m-2" type="grow" color="danger" />
          <Spinner className="m-2" type="grow" color="warning" />
          <Spinner className="m-2" type="grow" color="info" />
          <Spinner className="m-2" type="grow" color="dark" />
        </Col>
      </Row>
    </Container>
  );
};

export default ExampleSpinners;
